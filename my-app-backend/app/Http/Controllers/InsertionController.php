<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class InsertionController extends Controller
{
    public function __construct() {}

    public function index()
    {
        return response()->json(['error' => 'forbidden'], 403);
    }

    public function insertClass(Request $request)
    {
        $classInformation = $request->only('class_id', 'class_name');

        DB::table('class_entries')->insert(
            ['id' => null, 'class_name' => $classInformation['class_name'], 'class_number' => $classInformation['class_id']]
        );
    }

    public function bookClass(Request $request)
    {
        $classInformation = $request->only('user_id', 'class_id');

        // If the class exists...
        if (DB::table('class_entries')->select('class_number')->where('class_number', $classInformation['class_id'])->exists())
        {
            // And if the requester's school ID is not associated with that class already...
            if (!DB::table('user_entries')->where('school_id', $classInformation['user_id'])->where('class_id', $classInformation['class_id'])->exists())
            {
                // Add entry!
                DB::table('user_entries')->insert(
                    ['id' => null, 'school_id' => $classInformation['user_id'], 'class_id' => $classInformation['class_id']]
                );
                return response()->json(['result' => 'Success! You have booked this course.']);
            }
            else
            {
                return response()->json(['result' => 'ERROR: You are already enrolled in this course!']);
            }
        }
        else
        {
            return response()->json(['result' => 'ERROR: Class does not exist!']);
        }
    }
}
