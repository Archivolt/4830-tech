<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RemovalController extends Controller
{
    public function __construct() {}

    public function index()
    {
        return response()->json(['error' => 'forbidden'], 403);
    }

    public function removeClass(Request $request)
    {
        $classInformation = $request->only('class_id');

        DB::table('class_entries')->where('id', $classInformation['class_id'])->delete();

        return response()->json(['result' => true]);
    }
}
