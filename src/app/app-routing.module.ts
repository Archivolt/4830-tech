import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// Components
import { SigninComponent } from './signin/signin.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { Notfound404Component } from './notfound-404/notfound-404.component';

// Routes
const routes: Routes = [
  { path: '', component: SigninComponent }, // Index
  { path: 'dashboard/:id', component: MainDashboardComponent }, // Main Application Page
  { path: '**', component: Notfound404Component }, // 404
];

@NgModule({
  exports: [ RouterModule ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
