import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';

// Material Components
import { MatCardModule, MatFormFieldModule, MatIconModule, MatInputModule, MatButtonModule, MatSidenavModule, MatMenuModule, MatToolbarModule, MatTableModule} from '@angular/material';
import { AppRoutingModule } from './/app-routing.module';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { Notfound404Component } from './notfound-404/notfound-404.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    MainDashboardComponent,
    Notfound404Component
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    AppRoutingModule,
    MatSidenavModule,
    MatMenuModule,
    MatToolbarModule,
    MatTableModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
