import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.css']
})
export class MainDashboardComponent implements OnInit {

  allClasses: string[];
  myClasses: string[];
  myID: number;

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit() {
    // Grab URL Parameter
    this.route.params.subscribe(params => { this.myID = + params['id']; });
    // Run data population
    this.retrieveAllClasses();
    this.myBookedClasses();
  }

  retrieveAllClasses()
  {
    this.http.get('https://tech-api.thetextbookexchange.club/get').subscribe(data => {
      // Read the result field from the JSON response.
      this.allClasses = data['classes'];
      this.formatAllClasses();
    });
  }
  formatAllClasses()
  {
    var temp = "<table><tr><th>Class ID</th><th>Class Name</th><th>Class Number</th></tr><tr>";
    for (var i = 0; i < this.allClasses.length; i++) {
      temp = temp
        + "<td>" + JSON.stringify((this.allClasses[i] as any).id) + "</td>"
        + "<td>" + JSON.stringify((this.allClasses[i] as any).class_name) + "</td>"
        + "<td>" + JSON.stringify((this.allClasses[i] as any).class_number) + "</td>"
        + "</tr></table>";
    }
    document.getElementById("allClassesTable").innerHTML = temp;
  }

  deleteClass()
  {
    var msg = (document.getElementById('deleteClass') as any).value;
    var body = {class_id: msg};
    this.http.post('https://tech-api.thetextbookexchange.club/remove/class', body).subscribe();
    location.reload();
  }

  addClass()
  {
    var op1 = (document.getElementById('className') as any).value
    var op2 = (document.getElementById('classID') as any).value;
    var body = {class_id: op2, class_name: op1};
    this.http.post('https://tech-api.thetextbookexchange.club/insert/class', body).subscribe();
    location.reload();
  }

  myBookedClasses()
  {
    var body = {school_id: this.myID};
    this.http.post('https://tech-api.thetextbookexchange.club/get/class', body).subscribe(data => {
      this.myClasses = data['classes'];
      this.formatMyClasses();
    });
  }
  formatMyClasses()
  {
    var temp = "";
    for (var i = 0; i < this.myClasses.length; i++) {
      temp = temp + "<tr>"
        + "<td><h4 class=\"mat-display-1\">" + JSON.stringify((this.myClasses[i] as any).class_id) + "</h4></td>"
        + "</tr>";
    }
    document.getElementById("myClassesTable").innerHTML = temp;
  }

  bookClass()
  {
    var op1 = (document.getElementById('bookclassID') as any).value;
    var op2 = this.myID;
    var body = {user_id: op2, class_id: op1};
    this.http.post('https://tech-api.thetextbookexchange.club/bookclass', body).subscribe();
    location.reload();
  }

  searchClass()
  {
    var tempInput = (document.getElementById('searchclassID') as any).value;
    var body = {class_name: tempInput};
    this.http.post('https://tech-api.thetextbookexchange.club/get/class/search', body).subscribe(data => {
      console.log(data['classes']);
      if ((data['classes'] as any) <= 1)
      {
        console.log("DOES NOT EXIST");
        document.getElementById("searchMessage").innerHTML = "Unable to find a course with the title: '" + tempInput + "'. Be sure you are typing in the course name instead of course number.";
        document.getElementById("searchResult").innerHTML = "";
      }
      else
      {
        console.log("EXISTS");
        document.getElementById("searchMessage").innerHTML = tempInput + " found!";
        document.getElementById("searchResult").innerHTML = "Please book with the course number: " + JSON.stringify((data['classes'][0] as any).class_number);
      }
    });
  }
}
