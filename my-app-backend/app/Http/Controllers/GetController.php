<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class GetController extends Controller
{
    public function __construct() {}

    public function index()
    {
        return response()->json(['error' => 'forbidden'], 403);
    }

    public function getAllClass()
    {
        $allEntries = DB::table('class_entries')->get();
        return response()->json(['classes' => $allEntries]);
    }

    public function getMyClasses(Request $request)
    {
        $myEntries = DB::table('user_entries')->where('school_id', $request['school_id'])->get();
        return response()->json(['classes' => $myEntries]);
    }

    public function getSpecificClass(Request $request)
    {
        $classEntries = DB::table('class_entries')->where('class_name', $request['class_name'])->get();
        return response()->json(['classes' => $classEntries]);
    }
}
