<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('get', 'GetController@getAllClass');
$router->get('get/class/all', 'GetController@getAllClass');
$router->post('get/class/search', 'GetController@getSpecificClass');
$router->post('get/class', 'GetController@getMyClasses');

$router->post('insert/class', 'InsertionController@insertClass');
$router->post('bookclass', 'InsertionController@bookClass');
$router->post('remove/class', 'RemovalController@removeClass');