import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  hide = true;
  dashboardlink = '';

  constructor() { }

  ngOnInit() {
  }

  goSignin(value: string) {
    this.dashboardlink = 'dashboard/' + value;
  }
}
